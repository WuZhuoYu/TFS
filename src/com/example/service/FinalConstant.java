package com.example.service;

public class FinalConstant {
	// 用户账户
	public static final String USERNAME = "admin";
	public static final String USERNAME0 = "admin";
	public static final String PASSWORD = "admin123";
	
	// 系统超时时间
	public static final long WAIT_TIME = 10000;
	
	// HTTP通信相关
	public static final String ENCODING = "GB2312";
	public static final String QUERY_DATA = "queryData";
	public static final String QUERY_STATE = "queryState";
	public static final String QUERY_PARAMS = "queryParams";
	public static final String CONTROL = "control";
	public static final String SET_PARAMS = "setParams";
	
	public static final String SENSOR_DATA_HEAD = "SENSOR";
	public static final String CONTROL_STATE_HEAD = "EQUIPMENT";
	public static final String PARAMS_DATA_HEAD = "PARAMS";
	public static final String CONTROL_SUCCESS = "success";
	
	public static final int[] EQUIPMENT = {0, 1, 2, 3, 4};
	public static final int ON = 1;
	public static final int OFF = 0;
	
	public static final int QUERY_BACK_DATA = 1;
	public static final String BACK_INFO = "backMessageFromServer";
	public static final int CONTROL_BACK_DATA = 2;
	public static final String CONTROL_INFO = "controlbackMessageFromServer";
	
	public static final int TIME_OUT = 3;
	
	public static final String PC_BACK_INFO = "backMessageFromServer";
	public static final int PC_QUERY_BACK_DATA = 1;
	
	/**批次下拉菜单请求信息*/
	public static final int GT_QUERY_BACK_DATA = 1;
	
	/**批次下拉菜单返回信息*/
	public static final String GT_BACK_INFO = "backMessageFromServer";
	
	/**提示请求信息*/
	public static final int NOTICE_QUERY_BACK_DATA = 1;	
	
	/** 返回提示 */
	public static final String NOTICE_BACK_INFO = "backMessageFromServer";
	
	/**监测点1查询请求服务器的命令信息*/
	public static  final  String MP1FIND_REQUEST_SERVER = "MP1SHOW";
	
	/**监测点1查询请求服务器的返回命令信息*/
	public static  final  String MP1FIND_REBACK_SERVER = "MP1SHOW_REBACK";
	
	
	/**监测点2查询请求服务器的命令信息*/
	public static  final  String MP2FIND_REQUEST_SERVER = "MP2SHOW";
	
	/**监测点2查询请求服务器的返回命令信息*/
	public static  final  String MP2FIND_REBACK_SERVER = "MP2SHOW_REBACK";
	
	/**监测点3查询请求服务器的命令信息*/
	public static  final  String MP3FIND_REQUEST_SERVER = "MP3SHOW";
	
	/**监测点3查询请求服务器的返回命令信息*/
	public static  final  String MP3FIND_REBACK_SERVER = "MP3SHOW_REBACK";
	
	
	/**气象站查询请求服务器的命令信息*/
	public static  final  String QXZFIND_REQUEST_SERVER = "QXZSHOW";
	
	/**气象站查询请求服务器的返回命令信息*/
	public static  final  String QXZFIND_REBACK_SERVER = "QXZSHOW_REBACK";
	
	/**大棚1查询请求服务器的命令信息*/
	public static  final  String DP1FIND_REQUEST_SERVER = "DP1SHOW";
	
	/**大棚1查询请求服务器的返回命令信息*/
	public static  final  String DP1FIND_REBACK_SERVER = "DP1SHOW_REBACK";
	
	/**种植信息查询请求服务器的命令信息*/
	public static  final  String PLANTFIND_REQUEST_SERVER = "PLANTSHOW";
	
	/**种植信息查询请求服务器的返回命令信息*/
	public static  final  String PLANTFIND_REBACK_SERVER = "PLANTSHOW_REBACK";
	
	/**种植信息提交请求服务器的命令信息*/
	public static final String PLANTSUBMIT_REQUEST_SERVER = "PLANTSUBMIT";
	
	/**种植信息提交请求服务器的返回命令信息*/
	public static final String PLANTSUBMIT_REBACK_SERVER = "PLANTSUBMIT_REBACK";
	
	/**农事信息查询请求服务器的命令信息*/
	public static  final  String FARMFIND_REQUEST_SERVER = "FARMSHOW";
	
	/**农事信息查询请求服务器的返回命令信息*/
	public static  final  String FARMFIND_REBACK_SERVER = "FARMSHOW_REBACK";
	
	/**农事信息提交请求服务器的命令信息*/
	public static final String FARMSUBMIT_REQUEST_SERVER = "FARMSUBMIT";
	
	/**农事信息提交请求服务器的返回命令信息*/
	public static final String FARMSUBMIT_REBACK_SERVER = "FARMSUBMIT_REBACK";
	
	/**硬件状态查询请求服务器的命令信息*/
	public static  final  String DEVICEFIND_REQUEST_SERVER = "DEVICEFIND";
	
	/**硬件状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICEFIND_REBACK_SERVER = "DEVICEFIND_REBACK";
	
	
	/**设备状态改变请求服务器的命令信息*/
	public static  final  String DEVICECHANGE_REQUEST_SERVER = "DEVICECHANGE";
	
	/**电磁阀1状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE1_REBACK_SERVER = "DEVICECHANGE1_REBACK";
	/**电磁阀2状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE2_REBACK_SERVER = "DEVICECHANGE2_REBACK";
	/**电磁阀3状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE3_REBACK_SERVER = "DEVICECHANGE3_REBACK";
	/**电磁阀4状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE4_REBACK_SERVER = "DEVICECHANGE4_REBACK";
	/**电磁阀5状态查询请求服务器的返回命令信息*/
	public static  final  String DEVICECHANGE5_REBACK_SERVER = "DEVICECHANGE5_REBACK";
	/**网络参数请求*/
	public static final String NETPARA_REQUEST_SERVER = "NETPARASHOW";
	/**网络参数请求返回*/
	public static final String NETPARA_REBACK_SERVER = "NETPARA_REBACK";
	/**病害诊断上传请求服务器命令*/
	public static final String EVENTSUBMIT_REQUEST_SERVER = "EVENTSUBMIT";
	/**病害诊断上传请求服务器命令*/
	public static final String EVENTSUBMIT_REBACK_SERVER = "EVENTSUBMIT_REBACK";
	
	/**农事信息施肥查询请求服务器的命令信息*/
	public static final String FARMFIND_FERTILIZATION_REQUEST_SERVER = "FARM_FERTILIZATION_SHOW";
	/**农事信息施肥查询请求服务器的返回命令信息*/
	public static final String FARMFIND_FERTILIZATION_REBACK_SERVER = "FARM_FERTILIZATION_SHOW_REBACK";
	/**农事信息施肥查询请求服务器的命令信息*/
	public static final String FARMFIND_FERTILIZATION_SUBMIT_SERVER = "FARM_FERTILIZATION_SUBMIT";
	/**农事信息施肥查询请求服务器的返回命令信息*/
	public static final String FARMFIND_FERTILIZATION_SUBMIT_REBACK_SERVER = "FARM_FERTILIZATION_SUBMIT_REBACK";
	
	/**农事信息整地查询请求服务器的命令信息*/
	public static final String FARMFIND_HARROW_REQUEST_SERVER = "FARM_HARROW_SHOW";
	/**农事信息整地查询请求服务器的返回命令信息*/
	public static final String FARMFIND_HARROW_REBACK_SERVER = "FARM_HARROW_SHOW_REBACK";
	/**农事信息整地查询请求服务器的命令信息*/
	public static final String FARMFIND_HARROW_SUBMIT_SERVER = "FARM_HARROW_SUBMIT";
	/**农事信息整地查询请求服务器的返回命令信息*/
	public static final String FARMFIND_HARROW_SUBMIT_REBACK_SERVER = "FARM_HARROW_SUBMIT_REBACK";
	
	/**农事信息定植查询请求服务器的命令信息*/
	public static final String FARMFIND_PLANTING_REQUEST_SERVER = "FARM_PLANTING_SHOW";
	/**农事信息定植查询请求服务器的返回命令信息*/
	public static final String FARMFIND_PLANTING_REBACK_SERVER = "FARM_PLANTING_SHOW_REBACK";
	/**农事信息定植查询请求服务器的命令信息*/
	public static final String FARMFIND_PLANTING_SUBMIT_SERVER = "FARM_PLANTING_SUBMIT";
	/**农事信息定植查询请求服务器的返回命令信息*/
	public static final String FARMFIND_PLANTING_SUBMIT_REBACK_SERVER = "FARM_PLANTING_SUBMIT_REBACK";
	
	/**农事信息灌溉查询请求服务器的命令信息*/
	public static final String FARMFIND_IRRIGATION_REQUEST_SERVER = "FARM_IRRIGATION_SHOW";
	/**农事信息灌溉查询请求服务器的返回命令信息*/
	public static final String FARMFIND_IRRIGATION_REBACK_SERVER = "FARM_IRRIGATION_SHOW_REBACK";
	/**农事信息灌溉查询请求服务器的命令信息*/
	public static final String FARMFIND_IRRIGATION_SUBMIT_SERVER = "FARM_IRRIGATION_SUBMIT";
	/**农事信息灌溉查询请求服务器的返回命令信息*/
	public static final String FARMFIND_IRRIGATION_SUBMIT_REBACK_SERVER = "FARM_IRRIGATION_SUBMIT_REBACK";
	
	/**农事信息追肥查询请求服务器的命令信息*/
	public static final String FARMFIND_TOPDRESSING_REQUEST_SERVER = "FARM_TOPDRESSING_SHOW";
	/**农事信息追肥查询请求服务器的返回命令信息*/
	public static final String FARMFIND_TOPDRESSING_REBACK_SERVER = "FARM_TOPDRESSING_SHOW_REBACK";
	/**农事信息追肥查询请求服务器的命令信息*/
	public static final String FARMFIND_TOPDRESSING_SUBMIT_SERVER = "FARM_TOPDRESSING_SUBMIT";
	/**农事信息追肥查询请求服务器的返回命令信息*/
	public static final String FARMFIND_TOPDRESSING_SUBMIT_REBACK_SERVER = "FARM_TOPDRESSING_SUBMIT_REBACK";
	
	/**农事信息防治查询请求服务器的命令信息*/
	public static final String FARMFIND_PREVENTION_REQUEST_SERVER = "FARM_PREVENTION_SHOW";
	/**农事信息防治查询请求服务器的返回命令信息*/
	public static final String FARMFIND_PREVENTION_REBACK_SERVER = "FARM_PREVENTION_SHOW_REBACK";
	/**农事信息防治查询请求服务器的命令信息*/
	public static final String FARMFIND_PREVENTION_SUBMIT_SERVER = "FARM_PREVENTION_SUBMIT";
	/**农事信息防治查询请求服务器的返回命令信息*/
	public static final String FARMFIND_PREVENTION_SUBMIT_REBACK_SERVER = "FARM_PREVENTION_SUBMIT_REBACK";
	
	/**农事信息除草查询请求服务器的命令信息*/
	public static final String FARMFIND_WEED_REQUEST_SERVER = "FARM_WEED_SHOW";
	/**农事信息除草查询请求服务器的返回命令信息*/
	public static final String FARMFIND_WEED_REBACK_SERVER = "FARM_WEED_SHOW_REBACK";
	/**农事信息除草查询请求服务器的命令信息*/
	public static final String FARMFIND_WEED_SUBMIT_SERVER = "FARM_WEED_SUBMIT";
	/**农事信息除草查询请求服务器的返回命令信息*/
	public static final String FARMFIND_WEED_SUBMIT_REBACK_SERVER = "FARM_WEED_SUBMIT_REBACK";
	
	/**农事信息授粉查询请求服务器的命令信息*/
	public static final String FARMFIND_POLLINATION_REQUEST_SERVER = "FARM_POLLINATION_SHOW";
	/**农事信息授粉查询请求服务器的返回命令信息*/
	public static final String FARMFIND_POLLINATION_REBACK_SERVER = "FARM_POLLINATION_SHOW_REBACK";
	/**农事信息授粉查询请求服务器的命令信息*/
	public static final String FARMFIND_POLLINATION_SUBMIT_SERVER = "FARM_POLLINATION_SUBMIT";
	/**农事信息授粉查询请求服务器的返回命令信息*/
	public static final String FARMFIND_POLLINATION_SUBMIT_REBACK_SERVER = "FARM_POLLINATION_SUBMIT_REBACK";
	
	/**农事信息采收查询请求服务器的命令信息*/
	public static final String FARMFIND_HARVEST_REQUEST_SERVER = "FARM_HARVEST_SHOW";
	/**农事信息采收查询请求服务器的返回命令信息*/
	public static final String FARMFIND_HARVEST_REBACK_SERVER = "FARM_HARVEST_SHOW_REBACK";
	/**农事信息采收查询请求服务器的命令信息*/
	public static final String FARMFIND_HARVEST_SUBMIT_SERVER = "FARM_HARVEST_SUBMIT";
	/**农事信息采收查询请求服务器的返回命令信息*/
	public static final String FARMFIND_HARVEST_SUBMIT_REBACK_SERVER = "FARM_HARVEST_SUBMIT_REBACK";
	/**在线人数查询请求服务器命令*/
	public static final String FIND_PEOPLE_ONLINE_REQUEST_SERVER = "PEOPLE_ONLINE_SHOW";
	/**在线人数查询请求服务器返回命令*/
	public static final String FIND_PEOPLE_ONLINE_REBACK_SERVER = "PEOPLE_ONLINE_SHOW_REBACK";
	
}

