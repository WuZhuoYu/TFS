package com.example.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.mysql.jdbc.ResultSetMetaData;

public class JdbcConnDataBase {
    
	private static final  String USERNAME="root";//登录数据库的用户名
	private static final String PASSWORD="";//登录数据库的密码
	private static final String hostName="192.168.1.109";//数据库主机IP
	private static final String port="3306";//数据库使用的端口
	private static final String dbName="yantingdb";//数据库名称
	private static final String Driver="com.mysql.jdbc.Driver";//jdbc驱动
	private static final String URL="jdbc:mysql://"+hostName+":"+port+"/"+dbName;
	private Connection connection;
	private PreparedStatement PS;
	private ResultSet resultSet;
	
	//加载数据库驱动
	public JdbcConnDataBase() {
		// TODO Auto-generated constructor stub
		try {
			Class.forName(Driver);
			System.out.println("注册成功");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	//获得数据库的链接
	/**
	 * 
	 * @return Connection
	 */
	public Connection getConnection(){
		try {
			connection=DriverManager.getConnection(URL, USERNAME, PASSWORD);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return connection;
		
	}
    //数据库的更新
	/**
	 * 
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public boolean updateByPreparedStatement(String sql,List<Object> params) 
			throws SQLException{
		           boolean flag=false;
		           int result=-1;
		           PS=connection.prepareStatement(sql);
		           int index=1;
		           if(params!=null&&!params.isEmpty()){
		        	   for(int i=0;i<params.size();i++){
		        		   PS.setObject(index++, params.get(i));   
		        	   }
		           }
		    result=PS.executeUpdate();
		    flag=result>0?true:false;
		return flag;	
	}
	/**
	 * 
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	//数据库的单行查询
	public Map<String,Object> querySimgleResult(String sql,List<Object> params)
	throws SQLException{
		Map<String,Object> map=new HashMap<String,Object>();
		int index=1;
		PS=connection.prepareStatement(sql);
		if(params!=null&&!params.isEmpty()){
			for(int i=0;i<params.size();i++){
     		   PS.setObject(index++, params.get(i));   
     	   }
		}
		resultSet=PS.executeQuery();
		ResultSetMetaData metaData=(ResultSetMetaData) resultSet.getMetaData();
		int col_len=metaData.getColumnCount();
		while(resultSet.next()){
			for(int i=0;i<col_len;i++){
				String col_name=metaData.getColumnName(i+1);
				Object col_value=resultSet.getObject(col_name);
				if(col_value==null){
					col_value="";
				}
				map.put(col_name, col_value);
			}
		}
		return map;
		
	}
	
	/**
	 * 
	 * @param sql
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	//数据库的多行查询
	public List<Map<String,Object>> queryMultiResult(String sql,List<Object> params)
	throws SQLException{
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		int index=1;
		PS=connection.prepareStatement(sql);
		if(params!=null&&!params.isEmpty()){
			for(int i=0;i<params.size();i++){
     		   PS.setObject(index++, params.get(i));   
     	   }
		}
		resultSet=PS.executeQuery();
		ResultSetMetaData metaData=(ResultSetMetaData) resultSet.getMetaData();
		int col_len=metaData.getColumnCount();
		while(resultSet.next()){
			Map<String,Object> map=new HashMap<String,Object>();
			for(int i=0;i<col_len;i++){
				String col_name=metaData.getColumnName(i+1);
				Object col_value=resultSet.getObject(col_name);
				if(col_value==null){
					col_value="";
				}
				map.put(col_name, col_value);
			}
			list.add(map);
		}
		return list;
		
	}
	/**
	 * 根据id删除表数据,前提是对应的id记录存在
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public boolean deleteData(String sql) throws SQLException{
		boolean isFinish;
		PS=connection.prepareStatement(sql);
		isFinish=PS.execute();
		return isFinish;
	}
	
	//关闭JDBC，释放资源
	public void releaseJDBC(){
		if(resultSet!=null){
			try {
				resultSet.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(PS!=null){
			try {
				PS.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(connection!=null){
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
