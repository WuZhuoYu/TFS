package com.example.YnkjApp;
/**
@Name:农事信息除草展示界面 *
@Description: * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-9-20 *
*/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class Weed_Activity extends Activity {

	
	private PreferencesService preservice;
	private String   cmd;
	private int count = 1;
	private ListView Lv_weed;

	private String IP = "120.79.76.116:7070";
	private HashMap<String, Object> reqparams;
	/**线程状态*/
	private   boolean nThread= true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.farm_weed_activity);
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		findViewById(R.id.new_project).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(Weed_Activity.this,Weed_AddInfo_Activity.class);
				startActivity(intent);
			}
		});
		Lv_weed = (ListView) findViewById(R.id.Lv_weed);


		// 获取网络配置
		preservice = new PreferencesService(getApplicationContext());
		Map<String, String> params = preservice.getPreferences();
		String serviceIP = params.get("serviceIP");

		if (serviceIP.equals("")) {
			preservice.save("120.79.76.116:8000", "222.180.45.174", "8083", "admin", "zs123456");
		} else {
			IP = serviceIP;

		}
		
		cmd = FinalConstant.FARMFIND_WEED_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
 		reqparams.put("cmd", cmd);
		new Thread(query).start();

	}
	//子线程
		private Runnable query = new Runnable() {

			@Override
			public void run() {
				while (nThread){
					try {
						String path ="http://"+IP+"/AppService.php";
	 					
	 					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
	 					Log.d("debugTest","reqdata -- "+reqdata);
	 					if (reqdata!=null) {
	 						//子线程用sedMessage()方法传弟)Message对象
								Message msg = mhandler.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
								Bundle bundle = new Bundle();//创建一个句柄
							    bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);//将reqdata填充入句柄
							    msg.setData(bundle);//设置一个任意数据值的Bundle对象。
							    mhandler.sendMessage(msg);
						}
	 					Thread.sleep(1000);//线程暂停1秒，单位毫秒  启动线程后，线程每10s发送一次消息
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}
			
		};
		private Handler mhandler = new Handler() {
			private String i;

			@SuppressLint("HandlerLeak")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == FinalConstant.QUERY_BACK_DATA) {
					String jsonData = msg.getData().getString(FinalConstant.BACK_INFO);
					try {
							if(jsonData.equals("1"))
							{
								Toast.makeText(Weed_Activity.this, "服务器没有开启或异常", Toast.LENGTH_LONG).show();
							}else{
								JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
							    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
								String str_cmd= tmp_cmd.getString("cmd"); 
							    Log.d("debugTest","arr_data -- "+arr);
							    int len = 0;
							    len = arr.length();
							    Log.d("debugTest","len -- "+len);
							    if(len>1)
							    {
							    	 if(str_cmd.equals(FinalConstant.FARMFIND_WEED_REBACK_SERVER)){
							    		 JsListviewShowDh(arr);	
							    	 }
							    }
							}
							
					} catch (JSONException e) {
							e.printStackTrace();
						}			
				}
		    }

			private void JsListviewShowDh(JSONArray arr) {
				try {
					//获取json数组对象有效数据
					if(!arr.get(1).equals(false)){
						JSONArray arr_data = (JSONArray) arr.get(1);
						List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
						Log.d("debugTest","--------打印数据 ------ "+arr_data); 
						int len = 0;
						len = arr_data.length();
						for(int i=0;i<len;i++)
						{
							JSONObject temp = (JSONObject) arr_data.get(i);
							HashMap<String, Object> item = new HashMap<String, Object>();
							item.put("x", Integer.toString(i+1));
							item.put("weed_time", temp.getString("weed_time"));
							item.put("weed_type", temp.getString("weed_type"));
							data.add(item);
						}
						//R.layout.item_death指layout文件夹下面的item_death.xml，
						SimpleAdapter	adapter = new SimpleAdapter(Weed_Activity.this,data,R.layout.item_farm_weed,
								new String[]{"x","weed_time","weed_type"},
								new int[]{R.id.edit,R.id.weed_time,R.id.weed_type});//适配器把数据绑定到界面里面的显示控件上
						Lv_weed.setAdapter(adapter); //把数据传给listview，得到数据总数，显示
					 }
					
			    } catch (JSONException e) {
					e.printStackTrace();
				}
				
			
				
			};
		};
		
}
