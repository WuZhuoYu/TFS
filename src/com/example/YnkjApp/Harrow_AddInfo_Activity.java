package com.example.YnkjApp;

/**
@Name:农事信息整地添加界面 *
@Description: * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-9-20 *
*/
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PlantService;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class Harrow_AddInfo_Activity extends Activity {

	private EditText harrow_time;
	private EditText harrow_location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.farm_harrow_add_info_activity);

		// 返回
		findViewById(R.id.back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		harrow_time = (EditText) findViewById(R.id.et_harrow_time);
		harrow_location = (EditText) findViewById(R.id.et_harrow_location);

		// 时间选择器
		harrow_time.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					showDatePickDlg();
					return true;
				}
				return false;
			}

			private void showDatePickDlg() {
				Calendar calendar = Calendar.getInstance();
				DatePickerDialog datePickerDialog = new DatePickerDialog(Harrow_AddInfo_Activity.this,
						new OnDateSetListener() {

							@Override
							public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
								Harrow_AddInfo_Activity.this.harrow_time
										.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
							}
						}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
						calendar.get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show();

			}
		});
		findViewById(R.id.Save).setOnClickListener(new OnClickListener() {

			private String cmd;
			private HashMap<String, Object> reqparams;

			@Override
			public void onClick(View v) {
				Save();

			}

			private void Save() {
				if ((harrow_time.length() != 0) || (harrow_location.length() != 0)) {
					try {
						cmd = FinalConstant.FARMFIND_HARROW_SUBMIT_SERVER;
						reqparams = new HashMap<String, Object>();
						reqparams.put("cmd", cmd);
						reqparams.put("harrow_time", URLEncoder.encode(harrow_time.getText().toString(), "UTF-8"));
						reqparams.put("harrow_location",
								URLEncoder.encode(harrow_location.getText().toString(), "UTF-8"));

						new Thread(query).start();
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
				} else {
					Toast.makeText(getApplicationContext(), "编辑框不能为空！", Toast.LENGTH_LONG).show();
				}
			}

			private Runnable query = new Runnable() {
				private String serverIP = "120.79.76.116:7070";

				@Override
				public void run() {
					PlantService serviceip;
					serviceip = new PlantService(getApplicationContext());
					Map<String, String> params = serviceip.getPreferences();
					String url = params.get("serviceip");
					String path = "";
					if (url.equals("")) {
						path = "http://" + serverIP + "/AppService.php";
					} else {
						path = "http://" + serverIP + "/AppService.php";
					}
					try {
						String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
						Log.d("debugTest", "reqdata -- " + reqdata);
						if (reqdata != null) {
							// 子线程用sedMessage()方法传弟)Message对象
							Message msg = mhandler.obtainMessage(FinalConstant.QUERY_BACK_DATA);
							Bundle bundle = new Bundle();// 创建一个句柄
							bundle.putString(FinalConstant.BACK_INFO, reqdata);// 将reqdata填充入句柄
							msg.setData(bundle);// 设置一个任意数据值的Bundle对象。
							mhandler.sendMessage(msg);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				private Handler mhandler = new Handler() {

					@Override
					public void handleMessage(Message msg) {
						if (msg.what == FinalConstant.QUERY_BACK_DATA) {
							Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_LONG).show();
							harrow_time.setText("");
							harrow_location.setText("");
						}
					}

				};

			};
		});

	}
}
