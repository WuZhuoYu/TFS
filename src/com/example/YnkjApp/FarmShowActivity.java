package com.example.YnkjApp;

/**
@Name:农事信息选择界面 *
@Description: * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-9-20 *
*/

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class FarmShowActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.farm_showactivity);

		findViewById(R.id.farmshow_back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		findViewById(R.id.ll_fertilization).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Fertilization_Activity.class);
				startActivity(intent);

			}
		});

		findViewById(R.id.ll_Harrow).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Harrow_Activity.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.ll_planting).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Planting_Activity.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.ll_irrigation).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Irrigation_Activity.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.ll_topdressing).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Topdressing_Activity.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.ll_prevention).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Prevention_Activity.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.ll_weed).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Weed_Activity.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.ll_pollination).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Pollination_Activity.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.ll_harvest).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(FarmShowActivity.this, Harvest_Activity.class);
				startActivity(intent);
			}
		});
	}

}
