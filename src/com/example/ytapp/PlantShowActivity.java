package com.example.ytapp;
/**
@Name:种植信息展示界面 *
@Description: * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-9-20 *
*/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.YnkjApp.R;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PreferencesService;


public class PlantShowActivity extends Activity {
	/**返回*/
    private TextView back;
    private TextView change;
	private ListView listViewPlant;
	private EditText time;
	private EditText variety;
	private EditText area;
	private EditText age;
	private EditText substrate,engding_farm;
	private EditText seeding_method;
	private Button   btn_paraSave;
	/**线程状态*/
	private   boolean nThread= true;
	private PreferencesService  preservice;
	/**服务器地址*/	
	private String serverIP = "192.168.1.148";
	private String   cmd;
	private Map<String, Object>  reqparams;
	int selectedRow = 0;  
    int ActivityID=1;
	private EditText ending_farm;  
   
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plantshow);
		listViewPlant = (ListView)this.findViewById(R.id.listViewPlant);
		time =  (EditText) this.findViewById(R.id.time);
		variety =  (EditText) this.findViewById(R.id.variety);
		area =  (EditText) this.findViewById(R.id.area);
		age =  (EditText) this.findViewById(R.id.age);
		substrate =  (EditText) this.findViewById(R.id.substrate);
		seeding_method =  (EditText) this.findViewById(R.id.seeding_method);
		ending_farm =  (EditText) this.findViewById(R.id.ending_farm);
		change  = (TextView) this.findViewById(R.id.new_project);
		change.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v){
				Intent intent=new Intent(PlantShowActivity.this,PlantActivity.class);
				startActivity(intent);
			}
			
		});
		
		cmd = FinalConstant.PLANTFIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
 		reqparams.put("cmd", cmd);
		new Thread(query).start();
		
		back  = (TextView) this.findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		//获取网络配置
				preservice = new PreferencesService(getApplicationContext());
			    Map<String, String> params = preservice.getPreferences();
			    String serviceIP = params.get("serviceIP");
			    if(serviceIP.equals("")){
			    	preservice.save("192.168.1.148",  "", "", "", "");
			    }else{
			    	serverIP = serviceIP;
			       
			    }
		
	};
	
	
	//子线程  每个10秒从服务器获取传感器数据
    private Runnable query = new Runnable() {
 			@Override
 			public void run() {
	 				while (nThread){	
			 			try{
			 				    String path ="http://"+serverIP+"/AppService.php";
		 					
			 					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
			 					Log.d("debugTest","reqdata -- "+reqdata);
			 					if(reqdata!= null){
			 							    //子线程用sedMessage()方法传弟)Message对象
			 								Message msg = mhandler.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
			 								Bundle bundle = new Bundle();//创建一个句柄
			 							    bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);//将reqdata填充入句柄
			 							    msg.setData(bundle);//设置一个任意数据值的Bundle对象。
			 							    mhandler.sendMessage(msg);
			 					 }
			 					Thread.sleep(1000);//线程暂停10秒，单位毫秒  启动线程后，线程每10s发送一次消息
			 			}catch(Exception e){
			 					e.printStackTrace();
			 			}
	 				}
		 			
 			};
 	};
		private Handler mhandler = new Handler() {
			@SuppressLint("HandlerLeak")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == FinalConstant.QUERY_BACK_DATA) {
					String jsonData = msg.getData().getString(FinalConstant.BACK_INFO);
					try {
							if(jsonData.equals("1"))
							{
								Toast.makeText(PlantShowActivity.this, "服务器没有开启或异常", Toast.LENGTH_LONG).show();
							}else{
								JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
							    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
								String str_cmd= tmp_cmd.getString("cmd"); 
							    Log.d("debugTest","arr_data -- "+arr);
							    int len = 0;
							    len = arr.length();
							    Log.d("debugTest","len -- "+len);
							    if(len>1)
							    {
							    	 //种植信息显示处理
								    if(str_cmd.equals(FinalConstant.PLANTFIND_REBACK_SERVER)){
								        JsListviewShowDh(arr);	
								    }
								    
							    }
							}
					} catch (JSONException e) {
							e.printStackTrace();
						}			
				}
		    };
		};
		/**
		 * listview种植信息显示
		 * @param arr
		 */
		private void JsListviewShowDh(JSONArray arr) {
			try {
				//获取json数组对象有效数据
				if(!arr.get(1).equals(false)){
					JSONArray arr_data = (JSONArray) arr.get(1);
					List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();
					Log.d("debugTest","JsTabhostShowDh -- "+arr_data); 
					int len = 0;
					len = arr_data.length();
					for(int i=0;i<len;i++)
					{
						JSONObject temp = (JSONObject) arr_data.get(i);
						HashMap<String, Object> item = new HashMap<String, Object>();
						item.put("x", Integer.toString(i+1));
						item.put("time", temp.getString("time"));
						item.put("variety", temp.getString("variety"));
						item.put("area", temp.getString("area"));
						item.put("age", temp.getString("age"));
						item.put("substrate", temp.getString("substrate"));
						item.put("seeding_method", temp.getString("seeding_method"));
						item.put("ending_farm", temp.getString("ending_farm"));
						data.add(item);
					}
					//R.layout.item_death指layout文件夹下面的item_death.xml，
					SimpleAdapter	adapter = new SimpleAdapter(PlantShowActivity.this,data,R.layout.itemplant,
							new String[]{"x","time","variety","area","age","substrate","seeding_method","ending_farm"},
							new int[]{R.id.edit,R.id.time,R.id.variety,R.id.area,R.id.age,R.id.substrate,R.id.seeding_method,R.id.ending_farm});//适配器把数据绑定到界面里面的显示控件上
					listViewPlant.setAdapter(adapter); //把数据传给listview，得到数据总数，显示
						
				 }
		    } catch (JSONException e) {
				e.printStackTrace();
			}				
		};
    
		
}
