package com.example.ytapp;
/**
@Name:环境参数展示界面 *
@Description: * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-9-20 *
*/
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.MediaPlayer.PlayM4.Player;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.YnkjApp.R;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PlaySurfaceView;
import com.example.service.PreferencesService;
import com.hikvision.netsdk.ExceptionCallBack;
import com.hikvision.netsdk.HCNetSDK;
import com.hikvision.netsdk.NET_DVR_COMPRESSIONCFG_V30;
import com.hikvision.netsdk.NET_DVR_DEVICEINFO_V30;
import com.hikvision.netsdk.NET_DVR_PREVIEWINFO;
import com.hikvision.netsdk.PlaybackCallBack;
import com.hikvision.netsdk.RealPlayCallBack;

/**
 * 环境监测窗口
 * @author Administrator
 *
 */
public class EnvironmentActivity extends Activity implements Callback{
	/**监测点选择*/
    private TextView        tv_content;
 	private ImageView       img_right;
 	
    /**返回*/
    private TextView back;
    private TextView peng_title;
    private String mYear ; // 获取当前年份  
    private String mMonth;// 获取当前月份  
    private String mDay;// 获取当前月份的日期号码
    
    /**苗圃布局*/
    private View include_changeOne;
    
    /**气象站布局*/
    private View include_changeTwo;
    
    /**温室站布局*/
    private View include_changeThree;   
    
    
    /**测试点1空气温度*/
    private TextView data1;
    
    /**测试点1空气湿度*/
    private TextView data2;
    
    /**测试点1CO2浓度*/
    private TextView data3;
    
    /**测试点1光照强度*/
    private TextView data4;
     
    
    /**气象站表层土壤水分*/
    private TextView qixz_data1;
    
    /**气象站浅层土壤水分*/
    private TextView qixz_data2;
    
    /**气象站中层土壤水分*/
    private TextView qixz_data3;
    
    /**气象站深层土壤水分*/
    private TextView qixz_data4;
    /**
     * <p>气象站空气温度</p>
     */
    private TextView qixz_data5;
       
    
    /**大棚空气温度*/
    private TextView dp_data1;
    
    /**大棚空气湿度*/
    private TextView dp_data2;
    
    /**大棚土壤温度*/
    private TextView dp_data3;
    
    /**大棚土壤湿度*/
    private TextView dp_data4;
    /**
     * <p>大棚光照强度</p>
     */
    private TextView dp_data5;
    
    /**大棚CO2浓度*/
    private TextView dp_data6;
    
    /**大棚全辐射*/
    private TextView dp_data7;
    
    /**大棚露点温度*/
    private TextView dp_data8;
    
    /**子线程提交命令*/
    private  String  cmd="";
    
    /**子线程组合参数*/
    private  Map<String, Object>  reqparams;
 	
 	/*视频参数*/
 	private ImageView       img_play;

	private SurfaceView 	m_osurfaceView			= null;

	private String      m_oIPAddr = "118.113.95.71";
	private String		m_oPort	="8081"				;
	private String		m_oUser ="admin";
	private String		m_oPsd = "cdjx1234";
	
	private NET_DVR_DEVICEINFO_V30 m_oNetDvrDeviceInfoV30 = null;
	
	/**监测点摄像头的通道号*/
	private int        m_iChan = 38;

	
	private int				m_iLogID				= -1;				// return by NET_DVR_Login_v30
	private int 			m_iPlayID				= -1;				// return by NET_DVR_RealPlay_V30
	private int				m_iPlaybackID			= -1;				// return by NET_DVR_PlayBackByTime	
	
	private int				m_iPort					= -1;				// play port
	private	int 			m_iStartChan 			= 0;				// start channel no
	private int				m_iChanNum				= 0;				//channel number
	private static PlaySurfaceView [] playView = new PlaySurfaceView[4];
	
	private final String 	TAG						= "MainActivity";
	
	private boolean			m_bMultiPlay			= false;
	
	private boolean			m_bNeedDecode			= true;
	
	/**加载视频缓冲页面*/
	private View startVideoAnimPage = null;
 	private ImageView loadingVideo = null;
 	private Animation loadingAnim = null;
	private final long SPLASH_LENGTH = 100;    
    Handler handler = new Handler();  
    
    /**切换大棚时加载页面缓冲*/
    private TextView loadTV = null;
	private View linkPage = null;
	private ImageView linkimage = null;
	
	/**线程状态*/
	private   boolean nThread= true;

	private PreferencesService  preservice;
	
	/**服务器地址*/	
	private String serverIP = "61.157.134.34:8000";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.environment);
		back  = (TextView) this.findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
//		InitVedioView(); //初始化视频组件
		InitDataView();	//初始化视频组件
 		cmd = FinalConstant.MP1FIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
        new Thread(query_int).start();
        
		//获取网络配置
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    String vedioIP = params.get("vedioIP");
	    String vedioPort = params.get("vedioPort");
	    String vedioUser = params.get("vedioUser");
	    String vedioPsw = params.get("vedioPsw");
	    if(serviceIP.equals("")){
	    	preservice.save("www.cdghny.com", "118.113.95.71", "8081", "admin", "cdjx1234");
	    }else{
	    	serverIP = serviceIP;
	        m_oIPAddr = vedioIP;
	    	m_oPort	= vedioPort;
	    	m_oUser = vedioUser;
	    	m_oPsd = vedioPsw;
	    }
	    
	};
	/**
	 * 初始化传感器组件
	 */
	private void InitDataView(){
		include_changeOne = this.findViewById(R.id.include_changeOne);//育苗
		include_changeTwo = this.findViewById(R.id.include_changeTwo);//气象站
		include_changeThree = this.findViewById(R.id.include_changeThree);//温室
 		data1 = (TextView) findViewById(R.id.data1);
 		data2 = (TextView) findViewById(R.id.data2);
 		data3 = (TextView) findViewById(R.id.data3);
 		data4 = (TextView) findViewById(R.id.data4);
 		qixz_data1 = (TextView) findViewById(R.id.tv_qxz_air_temp_data);
 		qixz_data2 = (TextView) findViewById(R.id.tv_qxz_air_damp_data);
 		qixz_data3 = (TextView) findViewById(R.id.tv_qxz_soil_temp_data);
 		qixz_data4 = (TextView) findViewById(R.id.tv_qxz_soil_damp_data);
 		qixz_data5 = (TextView) findViewById(R.id.tv_qxz_illumination_data);
 		dp_data1 = (TextView) findViewById(R.id.dp_data1);
 		dp_data2 = (TextView) findViewById(R.id.dp_data2);
 		dp_data3 = (TextView) findViewById(R.id.dp_data3);
 		dp_data4 = (TextView) findViewById(R.id.dp_data4);
 		dp_data5 = (TextView) findViewById(R.id.dp_data5);
 		dp_data6 = (TextView) findViewById(R.id.dp_data6);
 		dp_data7 = (TextView) findViewById(R.id.dp_data7);
 		dp_data8 = (TextView) findViewById(R.id.dp_data8);
 		getcurrentTime();	//获取系统时间
 		peng_title = (TextView) findViewById(R.id.peng_title);
 		peng_title.setText("今日("+mYear+"-"+mMonth+"-"+mDay+")_实时数据");
 		tv_content = (TextView) findViewById(R.id.e_content);
 		
 		
 		img_right = (ImageView) findViewById(R.id.e_right);
	    img_right.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//下拉列表
				AlertDialog.Builder builder = new AlertDialog.Builder(EnvironmentActivity.this);
	            builder.setTitle("选择类型");
	            String str_wd = "育苗1#育苗2#育苗3#气象站#温室大棚";
	            //指定下拉列表的显示数据
	            final String[] cities = str_wd.split("#");
	            //设置一个下拉的列表选择项
	            builder.setItems(cities, new DialogInterface.OnClickListener()
	            {
	                    @Override
	                    public void onClick(DialogInterface dialog, int which)
	                    {
	                    	tv_content.setText(cities[which]);
	                    	if(which==0){
	                    		peng1Page();
	                    	}
	                       	if(which==1){
	                    		peng2Page();
	                    	}
	                       	if(which==2){
	                    		peng3Page();
	                    	}
	                       	if(which==3){
	                    		peng4Page();
	                    	}
	                       	if(which==4){
	                    		peng5Page();
	                    	}
	                       	
	                    }

	             });
	             builder.show();
			}
		});		
//        loadTV = (TextView) this.findViewById(R.id.loadingTV);
//        linkPage = (View) this.findViewById(R.id.linkPage);
//	    linkimage = (ImageView) findViewById(R.id.loadingVideo);
	};
	/*
     * 获取系统当前时间
     */
	@SuppressLint("SimpleDateFormat")
	private void getcurrentTime() {
	    final Calendar c = Calendar.getInstance();  
	    c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));  
	    mYear = String.valueOf(c.get(Calendar.YEAR)); // 获取当前年份  
	    mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);// 获取当前月份  
	    mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));// 获取当前月份的日期号码
	};		
	/**
	 * 初始化视频组件
	 */
//	private void InitVedioView(){
//		img_play = (ImageView) findViewById(R.id.img_play);
//	    m_osurfaceView = (SurfaceView) findViewById(R.id.Sur_Player);        
//	    m_osurfaceView.getHolder().addCallback(this);
//	    img_play.setOnClickListener(Login_Listener);
//	    
//	    startVideoAnimPage = (View) this.findViewById(R.id.startVideoAnim);
//	    loadingVideo = (ImageView) findViewById(R.id.loadingVideo);
//
//	    loadingAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
//	    startVideoAnimPage.getBackground().setAlpha(0);
//	    //初始化SDK
//	    if (!initeSdk())
//	    {
//	       finish();
//	       return;
//	    }
//	    
//	};
 	/**
 	 * 监测点1
 	 */
 	private void peng1Page(){
 		nThread = true;	//query_int线程开启
//		linkPage.setVisibility(View.VISIBLE);	//显示切换页面缓冲
//        linkimage.startAnimation(loadingAnim);
//        loadTV.setText("正在加载监测点1当前环境数据......");
 		
 		if(m_iPlayID >= 0)
 		{
 			stopSinglePreview();	//停止前面画面的播放
 			m_iChan = m_iStartChan+5;	//获取监测点1摄像头通道号	
			startSinglePreview(m_iChan);	//播放监测点1视频画面
 		}else{
 			m_iChan = 38;	//获取监测点1摄像头通道号	
 		}
 		include_changeOne.setVisibility(View.VISIBLE);
    	include_changeTwo.setVisibility(View.GONE);
    	include_changeThree.setVisibility(View.GONE);
    	cmd = FinalConstant.MP1FIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
        new Thread(query_int).start();	//从服务器获取监测点1传感器参数
       
 	};
 	/**
 	 * 监测点2
 	 */
 	private void peng2Page(){
 		nThread = true;	//query_int线程开启
// 		linkPage.setVisibility(View.VISIBLE);	//显示切换页面缓冲
//        linkimage.startAnimation(loadingAnim);
//        loadTV.setText("正在加载监测点2当前环境数据......");
        
        if(m_iPlayID >= 0)
 		{
 			stopSinglePreview();	//停止前面画面的播放
 			m_iChan = m_iStartChan+6;	//获取监测点2摄像头通道号	
			startSinglePreview(m_iChan);	//播放监测点2视频画面
 		}else{
 			m_iChan = 39;	//获取监测点2摄像头通道号	
 		}
		include_changeOne.setVisibility(View.VISIBLE);
    	include_changeTwo.setVisibility(View.GONE);
    	include_changeThree.setVisibility(View.GONE);
    	cmd = FinalConstant.MP2FIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	new Thread(query_int).start();	//从服务器获取监测点2传感器参数
    	
 	};
 	/**
 	 * 监测点3
 	 */
 	private void peng3Page(){
 		nThread = true;	//query_int线程开启
// 		linkPage.setVisibility(View.VISIBLE);	//显示切换页面缓冲
//        linkimage.startAnimation(loadingAnim);
//        loadTV.setText("正在加载监测点3当前环境数据......");
        
        if(m_iPlayID >= 0)
 		{
 			stopSinglePreview();	//停止前面画面的播放
 			m_iChan = m_iStartChan+7;	//获取监测点3摄像头通道号	
			startSinglePreview(m_iChan);	//播放监测点3视频画面
 		}else{
 			m_iChan = 40;	//获取监测点2摄像头通道号	
 		}
 		
		include_changeOne.setVisibility(View.VISIBLE);
    	include_changeTwo.setVisibility(View.GONE);
    	include_changeThree.setVisibility(View.GONE);
    	cmd = FinalConstant.MP3FIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	new Thread(query_int).start();	
    	
 	};
 	/**
 	 * 监测点4气象站
 	 */
 	private void peng4Page(){
 		nThread = true;	//query_int线程开启
// 		linkPage.setVisibility(View.VISIBLE);	//显示切换页面缓冲
//        linkimage.startAnimation(loadingAnim);
//        loadTV.setText("正在加载监测点4当前环境数据......");
        
        if(m_iPlayID >= 0)
 		{
 			stopSinglePreview();	//停止前面画面的播放
 			m_iChan = m_iStartChan+8;	//获取监测点3摄像头通道号	
			startSinglePreview(m_iChan);	//播放监测点3视频画面
 		}else{
 			m_iChan = 41;	//获取监测点2摄像头通道号	
 		}
 		
 		include_changeOne.setVisibility(View.GONE);	
    	include_changeTwo.setVisibility(View.VISIBLE);
    	include_changeThree.setVisibility(View.GONE);
    	cmd = FinalConstant.QXZFIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	new Thread(query_int).start();	
    	
 	}; 
 	/**
 	 * 监测点5温室大棚
 	 */
 	private void peng5Page(){
 		nThread = true;	//query_int线程开启
// 		linkPage.setVisibility(View.VISIBLE);	//显示切换页面缓冲
//        linkimage.startAnimation(loadingAnim);
//        loadTV.setText("正在加载监测点5当前环境数据......");
        
        if(m_iPlayID >= 0)
 		{
 			stopSinglePreview();	//停止前面画面的播放
 			m_iChan = m_iStartChan+9;	//获取监测点3摄像头通道号	
			startSinglePreview(m_iChan);	//播放监测点3视频画面
 		}else{
 			m_iChan = 42;	//获取监测点2摄像头通道号	
 		}
 		
 		include_changeOne.setVisibility(View.GONE);	
    	include_changeTwo.setVisibility(View.GONE);
    	include_changeThree.setVisibility(View.VISIBLE);
    	cmd = FinalConstant.DP1FIND_REQUEST_SERVER;
        reqparams = new HashMap<String, Object>();	//组织参数
    	reqparams.put("cmd", cmd);
    	new Thread(query_int).start();	
    	
 	};
 	
 	
 	//子线程  每个10秒从服务器获取传感器数据
    private Runnable query_int = new Runnable() {
 			@Override
 			public void run() {
	 				while (nThread){	
			 			try{
			 				    String path ="http://"+serverIP+"/AppService.php";
		 					
			 					String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
			 					Log.d("debugTest","reqdata -- "+reqdata);
			 					if(reqdata!= null){
			 							    //子线程用sedMessage()方法传弟)Message对象
			 								Message msg = mhandler_get.obtainMessage(FinalConstant.GT_QUERY_BACK_DATA);
			 								Bundle bundle = new Bundle();//创建一个句柄
			 							    bundle.putString(FinalConstant.GT_BACK_INFO, reqdata);//将reqdata填充入句柄
			 							    msg.setData(bundle);//设置一个任意数据值的Bundle对象。
			 							    mhandler_get.sendMessage(msg);
			 					 }
			 					Thread.sleep(10000);//线程暂停10秒，单位毫秒  启动线程后，线程每10s发送一次消息
			 			}catch(Exception e){
			 					e.printStackTrace();
			 			}
	 				}
		 			
 			};
 	};
 	@SuppressLint("HandlerLeak")
 	private Handler mhandler_get = new Handler() {
 			@SuppressLint("HandlerLeak")
 			@Override
 			public void handleMessage(Message msg) {
 				if (msg.what == FinalConstant.GT_QUERY_BACK_DATA) {
 					String jsonData = msg.getData().getString(FinalConstant.GT_BACK_INFO);
 					try {
	 						if(jsonData.equals("1"))
							{
//	 							linkPage.setVisibility(View.GONE);
//	 			  				loadingVideo.clearAnimation();//清除动画	
								Toast.makeText(getApplicationContext(), "服务器没有开启或异常", Toast.LENGTH_LONG).show();
								nThread = false;
							}else{
//								linkPage.setVisibility(View.GONE);
//				  				loadingVideo.clearAnimation();//清除动画	
								JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
								Log.d("arr","arr -- "+arr);
								
							    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
								String str_cmd= tmp_cmd.getString("cmd"); 
							    int len = 0;
							    len = arr.length();
							    if(len>1)
							    {
								    //监测点显示处理
								    if(str_cmd.equals(FinalConstant.MP1FIND_REBACK_SERVER)||str_cmd.equals(FinalConstant.MP2FIND_REBACK_SERVER)||str_cmd.equals(FinalConstant.MP3FIND_REBACK_SERVER)){
								    	ShowPeng(arr);
								    }
								   
								    //气象站显示处理
								    if(str_cmd.equals(FinalConstant.QXZFIND_REBACK_SERVER))
								    {
								    	ShowQxz(arr);
								    }
								  //温室大棚显示处理
								    if(str_cmd.equals(FinalConstant.DP1FIND_REBACK_SERVER))
								    {
								    	ShowDapeng(arr);
								    }
								}
							}
 					    
 				    } catch (JSONException e) {
 						e.printStackTrace();
 					}				
 			   }
 			
 		    };

 		    /**
 		     * 监测点数据显示
 		     * @param arr
 		     */
 			private void ShowPeng(JSONArray arr) {
 				TextView[] data={data1,data2,data3,data4};
 					try {
 						    if(arr.get(1).equals(false)){
 						    	for(int i=0;i<data.length;i++)
 						    	{
 						    		data[i].setText("----");
 						    	}
 						    }
 						    if(!arr.get(1).equals(false))
 						    {
 						    	//获取json数组对象有效数据
 						    	JSONArray arr_data = (JSONArray) arr.get(1);
 						    	Log.d("json数组", "json数组"+arr_data);
 								JSONObject temp = (JSONObject) arr_data.get(0);
 								data[0].setText(temp.getString("gh_temp")+"℃");
 								data[1].setText(temp.getString("gh_humi")+"%RH");
 								data[2].setText(temp.getString("gh_C2O")+"ppm");
 								data[3].setText(temp.getString("gh_illuminance")+"lux");
 																				
 						    }
 			    } catch (JSONException e) {
 					e.printStackTrace();
 				}				
 			};
 			
 			/**
 		     * 气象站数据显示
 		     * @param arr
 		     */
 			private void ShowQxz(JSONArray arr) {
 						TextView[] qxz_data={qixz_data1,qixz_data2,qixz_data3,qixz_data4,qixz_data5};
 						try {
 							    if(arr.get(1).equals(false)){
 							    	for(int i=0;i<qxz_data.length;i++)
 							    	{
 							    		qxz_data[i].setText("----");
 							    	}
 							    }
 							    if(!arr.get(1).equals(false))
 							    {
 							    	//获取json数组对象有效数据
 							    	JSONArray arr_data = (JSONArray) arr.get(1);
 									JSONObject temp = (JSONObject) arr_data.get(0);
 									qxz_data[0].setText(temp.getString("gh_temp")+"℃");
 									qxz_data[1].setText(temp.getString("gh_humi")+"%RH");
 									qxz_data[2].setText(temp.getString("gh_solid_temp")+"℃");
 									qxz_data[3].setText(temp.getString("gh_water_content")+"%RH");
 									qxz_data[4].setText(temp.getString("gh_illuminance")+"lux");
 									
 							    }
 				    } catch (JSONException e) {
 						e.printStackTrace();
 					}				
 			}; 
 			/**
 		     * 大棚数据显示
 		     * @param arr
 		     */
 			private void ShowDapeng(JSONArray arr) {
 						TextView[] dp_data={dp_data1,dp_data2,dp_data3,dp_data4,dp_data5,dp_data6,dp_data7,dp_data8};
 						try {
 							    if(arr.get(1).equals(false)){
 							    	for(int i=0;i<dp_data.length;i++)
 							    	{
 							    		dp_data[i].setText("----");
 							    	}
 							    }
 							    if(!arr.get(1).equals(false))
 							    {
 							    	//获取json数组对象有效数据
 							    	JSONArray arr_data = (JSONArray) arr.get(1);
 									JSONObject temp = (JSONObject) arr_data.get(0);
 									dp_data[0].setText(temp.getString("gh_temp")+"℃");
 									dp_data[1].setText(temp.getString("gh_humi")+"%RH");
 									dp_data[2].setText(temp.getString("gh_solid_temp")+"℃");
 									dp_data[3].setText(temp.getString("gh_water_content")+"%RH");
 									dp_data[4].setText(temp.getString("gh_illuminance")+"Lux");
 									dp_data[5].setText(temp.getString("gh_C2O")+"ppm");
 									dp_data[6].setText(temp.getString("gh_rad")+"rad");
 									dp_data[7].setText(temp.getString("gh_dew_temp")+"℃");
 									
 							    }
 				    } catch (JSONException e) {
 						e.printStackTrace();
 					}				
 			};
    };

	/** 
     * @fn initeSdk
     * @author zhuzhenlei
     * @brief SDK init
     * @param NULL [in]
     * @param NULL [out]
     * @return true - success;false - fail
     */
    private boolean initeSdk()
	{
		//init net sdk
    	if (!HCNetSDK.getInstance().NET_DVR_Init())
    	{
    		Log.e(TAG, "HCNetSDK init is failed!");
    		return false;
    	}
    	HCNetSDK.getInstance().NET_DVR_SetLogToFile(3, "/mnt/sdcard/sdklog/",true);
    	return true;
	};
	 //@Override    
    public void surfaceCreated(SurfaceHolder holder) { 
    	m_osurfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
    	Log.i(TAG, "surface is created" + m_iPort); 
		if (-1 == m_iPort)
		{
			return;
		}
        Surface surface = holder.getSurface();
        if (true == surface.isValid()) {
        	if (false == Player.getInstance().setVideoWindow(m_iPort, 0, holder)) {	
        		Log.e(TAG, "Player setVideoWindow failed!");
        	}	
    	}      
    }        
    //@Override  
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {   
    }  
    //@Override  
    public void surfaceDestroyed(SurfaceHolder holder) {  
    	Log.i(TAG, "Player setVideoWindow release!" + m_iPort);
		if (-1 == m_iPort)
		{
			return;
		}
        if (true == holder.getSurface().isValid()) {
        	if (false == Player.getInstance().setVideoWindow(m_iPort, 0, null)) {	
        		Log.e(TAG, "Player setVideoWindow failed!");
        	}
        }
    } 
	@Override  
	protected void onSaveInstanceState(Bundle outState) {    
		outState.putInt("m_iPort", m_iPort);  
		super.onSaveInstanceState(outState);  
		Log.i(TAG, "onSaveInstanceState"); 
	}  
	@Override  
	protected void onRestoreInstanceState(Bundle savedInstanceState) {  
		m_iPort = savedInstanceState.getInt("m_iPort");  
		super.onRestoreInstanceState(savedInstanceState);  
		Log.i(TAG, "onRestoreInstanceState" ); 
	}  
    /**
     * 视频设备登录并播放
     * 
     */
    private Button.OnClickListener Login_Listener = new Button.OnClickListener() 
	{
		public void onClick(View v) 
		{
			try
			{
				img_play.setVisibility(View.GONE);	//隐藏播放按钮
	            startVideoAnimPage.setVisibility(View.VISIBLE);	//显示动画页面
	            loadingVideo.startAnimation(loadingAnim);
	        	
	            if(m_iLogID < 0)
  				{
  					//设备注册
  					m_iLogID = loginDevice();
  					if (m_iLogID < 0)
  					{
  						Log.e(TAG, "This device logins failed!");
  						return;
  					}
  					// get instance of exception callback and set
  					ExceptionCallBack oexceptionCbf = getExceptiongCbf();
  					if (oexceptionCbf == null)
  					{
  					    Log.e(TAG, "ExceptionCallBack object is failed!");
  					    return ;
  					}
  					if (!HCNetSDK.getInstance().NET_DVR_SetExceptionCallBack(oexceptionCbf))
  				    {
  				        Log.e(TAG, "NET_DVR_SetExceptionCallBack is failed!");
  				        return;
  				    }
  					Log.i(TAG, "Login sucess ****************************1***************************");
  					
  					handler.postDelayed(new Runnable() {  //使用handler的postDelayed实现延时跳转  
  			            
  			            public void run() {    
  			            	startVideoAnimPage.setVisibility(View.GONE);
  		  					loadingVideo.clearAnimation();//清除动画	
  		  					startSinglePreview(m_iChan);	//播放监测点1视频画面   
  			            }    
  			        }, SPLASH_LENGTH);//1秒后播放视频画面  
  					
  				}
  				else
  				{
  					// whether we have logout
  					if (!HCNetSDK.getInstance().NET_DVR_Logout_V30(m_iLogID))
  					{
  						Log.e(TAG, " NET_DVR_Logout is failed!");
  						return;
  					}
  					m_iLogID = -1;
  				}		  
			  
			} 
			catch (Exception err)
			{
				Log.e(TAG, "error: " + err.toString());
			}
		}
	};
	private void startSinglePreview(int m_iStartChan)
	{  
		if(m_iPlaybackID >= 0)
		{
			Log.i(TAG, "Please stop palyback first");
			return ;
		}
		RealPlayCallBack fRealDataCallBack = getRealPlayerCbf();
		if (fRealDataCallBack == null)
		{
		    Log.e(TAG, "fRealDataCallBack object is failed!");
            return ;
		}
		Log.i(TAG, "m_iStartChan:" +m_iStartChan);
		        
        NET_DVR_PREVIEWINFO previewInfo = new NET_DVR_PREVIEWINFO();
        previewInfo.lChannel = m_iStartChan;
        previewInfo.dwStreamType = 1; //substream
        previewInfo.bBlocked = 1;       
		// HCNetSDK start preview
        m_iPlayID = HCNetSDK.getInstance().NET_DVR_RealPlay_V40(m_iLogID, previewInfo, fRealDataCallBack);
		if (m_iPlayID < 0)
		{
		 	Log.e(TAG, "NET_DVR_RealPlay is failed!Err:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
		 	return ;
		}
		Log.i(TAG, "NetSdk Play sucess ***********************3***************************");										
	};
	/** 
     * @fn stopSinglePreview
     * @author zhuzhenlei
     * @brief stop preview
     * @param NULL [in]
     * @param NULL [out]
     * @return NULL
     */
	private void stopSinglePreview()
	{
		if ( m_iPlayID < 0)
		{
			Log.e(TAG, "m_iPlayID < 0");
			return;
		}
		
		//  net sdk stop preview
		if (!HCNetSDK.getInstance().NET_DVR_StopRealPlay(m_iPlayID))
		{
			Log.e(TAG, "StopRealPlay is failed!Err:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
			return;
		}
		
		m_iPlayID = -1;		
		stopSinglePlayer();
	};
	private void stopSinglePlayer()
	{
		Player.getInstance().stopSound();		
		// player stop play
		if (!Player.getInstance().stop(m_iPort)) 
        {
            Log.e(TAG, "stop is failed!");
            return;
        }	
		
		if(!Player.getInstance().closeStream(m_iPort))
		{
            Log.e(TAG, "closeStream is failed!");
            return;
        }
		if(!Player.getInstance().freePort(m_iPort))
		{
            Log.e(TAG, "freePort is failed!" + m_iPort);
            return;
        }
		m_iPort = -1;
	};
   
	
		
	/** 
     * @fn loginDevice
     * @author zhuzhenlei
     * @brief login on device
     * @param NULL [in]
     * @param NULL [out]
     * @return login ID
     */
	private int loginDevice()
	{
		// get instance
		m_oNetDvrDeviceInfoV30 = new NET_DVR_DEVICEINFO_V30();
		if (null == m_oNetDvrDeviceInfoV30)
		{
			Log.e(TAG, "HKNetDvrDeviceInfoV30 new is failed!");
			return -1;
		}
		String strIP = m_oIPAddr;
		int	nPort = Integer.parseInt(m_oPort);
		String strUser = m_oUser;
		String strPsd = m_oPsd;
		// call NET_DVR_Login_v30 to login on, port 8000 as default
		int iLogID = HCNetSDK.getInstance().NET_DVR_Login_V30(strIP, nPort, strUser, strPsd, m_oNetDvrDeviceInfoV30);
		if (iLogID < 0)
		{
			Log.e(TAG, "NET_DVR_Login is failed!Err:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
			return -1;
		}
		if(m_oNetDvrDeviceInfoV30.byChanNum > 0)
		{
			m_iStartChan = m_oNetDvrDeviceInfoV30.byStartChan;
			m_iChanNum = m_oNetDvrDeviceInfoV30.byChanNum;
		}
		else if(m_oNetDvrDeviceInfoV30.byIPChanNum > 0)
		{
			m_iStartChan = m_oNetDvrDeviceInfoV30.byStartDChan;
			m_iChanNum = m_oNetDvrDeviceInfoV30.byIPChanNum + m_oNetDvrDeviceInfoV30.byHighDChanNum * 256;
		}
		Log.i(TAG, "NET_DVR_Login is Successful!");
		
		return iLogID;
	};
	/** 
     * @fn paramCfg
     * @author zhuzhenlei
     * @brief configuration
     * @param iUserID - login ID [in]
     * @param NULL [out]
     * @return NULL
     */
	private void paramCfg(final int iUserID)
	{
		// whether have logined on
		if (iUserID < 0)
		{
			Log.e(TAG, "iUserID < 0");
			return;
		}		
		
		NET_DVR_COMPRESSIONCFG_V30 struCompress = new NET_DVR_COMPRESSIONCFG_V30();
		if(!HCNetSDK.getInstance().NET_DVR_GetDVRConfig(iUserID, HCNetSDK.NET_DVR_GET_COMPRESSCFG_V30, m_iStartChan, struCompress))
		{
			Log.e(TAG, "NET_DVR_GET_COMPRESSCFG_V30 failed with error code:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
		}
		else
		{
			Log.i(TAG, "NET_DVR_GET_COMPRESSCFG_V30 succ");
		}
		//set substream resolution to cif
	    struCompress.struNetPara.byResolution = 1;
	    if(!HCNetSDK.getInstance().NET_DVR_SetDVRConfig(iUserID, HCNetSDK.NET_DVR_SET_COMPRESSCFG_V30, m_iStartChan, struCompress))
	    {
	    	Log.e(TAG, "NET_DVR_SET_COMPRESSCFG_V30 failed with error code:" + HCNetSDK.getInstance().NET_DVR_GetLastError());
	    }
	    else
	    {
	    	Log.i(TAG, "NET_DVR_SET_COMPRESSCFG_V30 succ");
	    }
	};
	/**
     * @fn getExceptiongCbf
     * @author zhuzhenlei
     * @brief process exception
     * @param NULL [in]
     * @param NULL [out]
     * @return exception instance
     */
	private ExceptionCallBack getExceptiongCbf()
	{
	    ExceptionCallBack oExceptionCbf = new ExceptionCallBack()
        {
            public void fExceptionCallBack(int iType, int iUserID, int iHandle)
            {
            	System.out.println("recv exception, type:" + iType);
            }
        };
        return oExceptionCbf;
	};
	/** 
     * @fn getRealPlayerCbf
     * @author zhuzhenlei
     * @brief get realplay callback instance
     * @param NULL [in]
     * @param NULL [out]
     * @return callback instance
     */
	private RealPlayCallBack getRealPlayerCbf()
	{
	    RealPlayCallBack cbf = new RealPlayCallBack()
        {
             public void fRealDataCallBack(int iRealHandle, int iDataType, byte[] pDataBuffer, int iDataSize)
             {
            	// player channel 1
            	 EnvironmentActivity.this.processRealData(1, iDataType, pDataBuffer, iDataSize, Player.STREAM_REALTIME); 
             }
        };
        return cbf;
	};
	/** 
     * @fn getPlayerbackPlayerCbf
     * @author zhuzhenlei
     * @brief get Playback instance
     * @param NULL [in]
     * @param NULL [out]
     * @return callback instance
     */
	private PlaybackCallBack getPlayerbackPlayerCbf()
	{
		PlaybackCallBack cbf = new PlaybackCallBack()
        {            
			@Override
			public void fPlayDataCallBack(int iPlaybackHandle, int iDataType, byte[] pDataBuffer, int iDataSize)
			{
				// player channel 1
				EnvironmentActivity.this.processRealData(1, iDataType, pDataBuffer, iDataSize, Player.STREAM_FILE);	
			}
        };
        return cbf;
	};
	/** 
     * @fn processRealData
     * @author zhuzhenlei
     * @brief process real data
     * @param iPlayViewNo - player channel [in]
     * @param iDataType	  - data type [in]
     * @param pDataBuffer - data buffer [in]
     * @param iDataSize   - data size [in]
     * @param iStreamMode - stream mode [in]
     * @param NULL [out]
     * @return NULL
     */
	public void processRealData(int iPlayViewNo, int iDataType, byte[] pDataBuffer, int iDataSize, int iStreamMode)
	{
		if(!m_bNeedDecode)
		{
		//   Log.i(TAG, "iPlayViewNo:" + iPlayViewNo + ",iDataType:" + iDataType + ",iDataSize:" + iDataSize);
		}
		else
		{
			if(HCNetSDK.NET_DVR_SYSHEAD == iDataType)
		    {
		    	if(m_iPort >= 0)
	    		{
	    			return;
	    		}	    			
	    		m_iPort = Player.getInstance().getPort();	
	    		if(m_iPort == -1)
	    		{
	    			Log.e(TAG, "getPort is failed with: " + Player.getInstance().getLastError(m_iPort));
	    			return;
	    		}
	    		Log.i(TAG, "getPort succ with: " + m_iPort);
	    		if (iDataSize > 0)
	    		{
	    			if (!Player.getInstance().setStreamOpenMode(m_iPort, iStreamMode))  //set stream mode
	    			{
	    				Log.e(TAG, "setStreamOpenMode failed");
	    				return;
	    			}
	    			if (!Player.getInstance().openStream(m_iPort, pDataBuffer, iDataSize, 2*1024*1024)) //open stream
	    			{
	    				Log.e(TAG, "openStream failed");
	    				return;
	    			}
	    			if (!Player.getInstance().play(m_iPort, m_osurfaceView.getHolder())) 
	    			{
	    				Log.e(TAG, "play failed");
	    				return;
	    			}	
	    			if(!Player.getInstance().playSound(m_iPort))
					{
						Log.e(TAG, "playSound failed with error code:" + Player.getInstance().getLastError(m_iPort));
						return;
					}
	    		}
		    }
		    else
		    {
		    	if (!Player.getInstance().inputData(m_iPort, pDataBuffer, iDataSize))
    			{
//		    		Log.e(TAG, "inputData failed with: " + Player.getInstance().getLastError(m_iPort));
  		    	    for(int i = 0; i < 4000 && m_iPlaybackID >=0 ; i++)
		    		{
		    			if (!Player.getInstance().inputData(m_iPort, pDataBuffer, iDataSize))
		    				Log.e(TAG, "inputData failed with: " + Player.getInstance().getLastError(m_iPort));
		    			else
		    				break;
		    			try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							e.printStackTrace();
						
						}	    				
		    		}
		    	}

		    }		
		}
	    
	};
	/** 
     * @fn Cleanup
     * @author zhuzhenlei
     * @brief cleanup
     * @param NULL [in]
     * @param NULL [out]
     * @return NULL
     */
    public void Cleanup()
    {
        // release player resource
    	
    	Player.getInstance().freePort(m_iPort);
		m_iPort = -1;
        
        // release net SDK resource
		HCNetSDK.getInstance().NET_DVR_Cleanup();
    };
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
         switch (keyCode)
         {
         case KeyEvent.KEYCODE_BACK:
        	 	
        	  stopSinglePlayer();
        	  Cleanup();
              android.os.Process.killProcess(android.os.Process.myPid());
              break;
         default:
              break;
         }
     
         return true;
    };

}
