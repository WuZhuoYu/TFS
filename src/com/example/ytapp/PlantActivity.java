package com.example.ytapp;
/**
@Name:种植信息添加界面 *
@Description: * 
@author wuzhuoyu * 
@Version:V1.00 * 
@Create Date:2018-9-20 *
*/
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.YnkjApp.Irrigation_AddInfo_Activity;
import com.example.YnkjApp.R;
import com.example.service.FinalConstant;
import com.example.service.HttpReqService;
import com.example.service.PlantService;
import com.example.service.PreferencesService;


public class PlantActivity extends Activity {
	/**返回*/
    private TextView back;
	 
	private EditText time;
	private EditText variety;
	private EditText area;
	private EditText age;
	private EditText substrate;
	private EditText seeding_method;
	private Button   btn_paraSave;
	private PreferencesService  preservice;
	private String   cmd;
	private Map<String, Object>  reqparams;
	int selectedRow = 0;  
    int ActivityID=1;  
    /**服务器地址*/	
	private String serverIP = "192.168.1.148";

	private EditText ending_farm;
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plant);
		
		time =  (EditText) this.findViewById(R.id.time);
		variety =  (EditText) this.findViewById(R.id.variety);
		area =  (EditText) this.findViewById(R.id.area);
		age =  (EditText) this.findViewById(R.id.age);
		substrate =  (EditText) this.findViewById(R.id.substrate);
		seeding_method =  (EditText) this.findViewById(R.id.seeding_method);
		ending_farm = (EditText) findViewById(R.id.ending_farm);
		btn_paraSave =  (Button) this.findViewById(R.id.paraSave);
		btn_paraSave.setOnClickListener(new HsSubmitOnClickListener());
		back  = (TextView) this.findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		// 时间选择器
		ending_farm.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (event.getAction() == MotionEvent.ACTION_DOWN) {
							showDatePickDlg();
							return true;
						}
						return false;
					}

					private void showDatePickDlg() {
						Calendar calendar = Calendar.getInstance();
						DatePickerDialog datePickerDialog = new DatePickerDialog(PlantActivity.this,
								new OnDateSetListener() {

									@Override
									public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
										PlantActivity.this.ending_farm
												.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
									}
								}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
								calendar.get(Calendar.DAY_OF_MONTH));
						datePickerDialog.show();

					}
				});
		
		// 时间选择器
				time.setOnTouchListener(new OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								if (event.getAction() == MotionEvent.ACTION_DOWN) {
									showDatePickDlg();
									return true;
								}
								return false;
							}

							private void showDatePickDlg() {
								Calendar calendar = Calendar.getInstance();
								DatePickerDialog datePickerDialog = new DatePickerDialog(PlantActivity.this,
										new OnDateSetListener() {

											@Override
											public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
												PlantActivity.this.time
														.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
											}
										}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
										calendar.get(Calendar.DAY_OF_MONTH));
								datePickerDialog.show();

							}
						});
		//获取网络配置
		preservice = new PreferencesService(getApplicationContext());
	    Map<String, String> params = preservice.getPreferences();
	    String serviceIP = params.get("serviceIP");
	    if(serviceIP.equals("")){
	    	preservice.save("192.168.1.148", "", "", "", "");
	    }else{
	    	serverIP = serviceIP;
	       
	    }
	    
	
	};
	
	/**
	 * 种植信息提交
	 * @author Administrator
	 *
	 */
	private class HsSubmitOnClickListener implements View.OnClickListener{
		@Override  
	    public void onClick(View v) {
			if (((time.length()!=0)&&(variety.length()!=0)&&(area.length()!=0)&&(age.length()!=0)&&(substrate.length()!=0)&&(seeding_method.length()!=0))||(ending_farm.length()!=0)) {
				try {
					 cmd = FinalConstant.PLANTSUBMIT_REQUEST_SERVER;
		             reqparams = new HashMap<String, Object>();	//组织参数
		        	 reqparams.put("cmd", cmd);
		        	 reqparams.put("time", URLEncoder.encode(time.getText().toString().trim(),"UTF-8"));
		        	 reqparams.put("variety", URLEncoder.encode(variety.getText().toString().trim(),"UTF-8"));
		        	 reqparams.put("area", URLEncoder.encode(area.getText().toString().trim(),"UTF-8"));
		        	 reqparams.put("age", URLEncoder.encode(age.getText().toString().trim(),"UTF-8"));
		        	 reqparams.put("substrate", URLEncoder.encode(substrate.getText().toString().trim(),"UTF-8"));
		        	 reqparams.put("seeding_method", URLEncoder.encode(seeding_method.getText().toString().trim(),"UTF-8"));
		        	 reqparams.put("ending_farm", URLEncoder.encode(ending_farm.getText().toString().trim(),"UTF-8"));
		             new Thread(query).start();	
				} catch (UnsupportedEncodingException  e) {
					e.printStackTrace();
				}
			}else{
					Toast.makeText(getApplicationContext(), "文本框不能为空", 0).show();
					}
	        };
	};
	
	
	//子线程
		private Runnable query = new Runnable() {
			@Override
			public void run() {
				//获取参数
				PlantService  serviceip;
				serviceip = new PlantService(getApplicationContext());
			    Map<String, String> params = serviceip.getPreferences();
			    String url = params.get("serviceip");
			    String path = "";
			    if(url.equals("")){
			    	 path ="http://"+serverIP+"/AppService.php";
			    }else{
			    	path ="http://"+serverIP+"/AppService.php";
			    }
				Log.d("debugTest","path -- "+path);
				//String path ="http://120.76.166.185:8000/testapp.php";
				try{
					  String reqdata = HttpReqService.postRequest(path, reqparams, "GB2312");
					  Log.d("debugTest","reqdata -- "+reqdata);
					  if(reqdata!= null){
						  //子线程用sedMessage()方法传弟)Message对象
						  Message msg = mhandler.obtainMessage(FinalConstant.QUERY_BACK_DATA);
						  Bundle bundle = new Bundle();//创建一个句柄
				          bundle.putString(FinalConstant.BACK_INFO, reqdata);//将reqdata填充入句柄
				          msg.setData(bundle);//设置一个任意数据值的Bundle对象。
						  mhandler.sendMessage(msg);
				  }
				
				}catch(Exception e){
					e.printStackTrace();
				}
			};
		};
		private Handler mhandler = new Handler() {
			@SuppressLint("HandlerLeak")
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == FinalConstant.QUERY_BACK_DATA) {
					String jsonData = msg.getData().getString(FinalConstant.BACK_INFO);
					try {
							if(jsonData.equals("1"))
							{
								Toast.makeText(PlantActivity.this, "服务器没有开启或异常", Toast.LENGTH_LONG).show();
							}else{
								JSONArray arr = new JSONArray(jsonData);  //收到JSON数组对象解析
							    JSONObject tmp_cmd = (JSONObject) arr.get(0);	//获取json数组对象返回命令
								String str_cmd= tmp_cmd.getString("cmd"); 
							    Log.d("debugTest","arr_data -- "+arr);
							    int len = 0;
							    len = arr.length();
							    Log.d("debugTest","len -- "+len);
							    if(len>1)
							    {
//							    	Toast.makeText(getApplicationContext(), "进来", 0).show();
								    if(str_cmd.equals(FinalConstant.PLANTSUBMIT_REBACK_SERVER))
								    {
								    	
								    	JSONObject result_cmd = (JSONObject) arr.get(1);
								    	//if(result_cmd.getString("RESULT").equals("SUCCESS")){
								    		Toast.makeText(PlantActivity.this, R.string.success, Toast.LENGTH_LONG).show();
								    		time.setText("");
								    		variety.setText("");
								    		area.setText("");
								    		age.setText("");
								    		substrate.setText("");
								    		seeding_method.setText("");
								    		ending_farm.setText("");
								    	//}
								    }
							    }
							}
					} catch (JSONException e) {
							e.printStackTrace();
						}			
				}
		    };
		};

		private EditText engding_farm;
		
    
}
